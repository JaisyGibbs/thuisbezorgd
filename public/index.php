<?php
    include '../Core/config.php';
    if(@$_POST['ajax'] || @$_GET['ajax']){
        include "../Pages/" . $page . ".php";
    }else{ ?>
<!DOCTYPE html>
<html>
    <head>
        <?php include '../Core/head.php'; ?>
    </head>
    <body>

        <?php include '../Core/header.php'; ?>

        <?php include "../Pages/" . $page . ".php"; ?>

        <?php include '../Core/footer.php'; ?>

    </body>
</html>
<?php
    DB::close();
}
?>

<!-- <div class="errorcontainer">
            <?php echo App::displayErrors(); ?>
        </div> -->

$(document).ready(function(){
    bucket();
    load_data();
    function load_data(query)
    {
        $.ajax({
            url:"?page=search&ajax=true",
            method:"post",
            data:{query:query},
            success:function(data)
            {
                $('#result').html(data);
            }
        });
    }

    $('#search_text').keyup(function(){
        var search = $(this).val();
        if(search != '')
        {
            load_data(search);
        }
        else
        {
            load_data();
        }
    });
});


function bucket()
{
    $('.cart').unbind('click').click(function(event) {
        event.preventDefault();
        //alert($(this).data('url'));
        jQuery.ajax($(this).data('url'), {
            method: 'post',
            cache: false,
            // dataType: 'json',
        })
        .done(function(data) {
            if(data) {
                $('#bucket').html(data);
                bucket();
            }
        })
        .fail(function() {
            alert( "error" );
            bucket();
        });
    });
    $('.addItem').click(function(){
        var id = $(this).attr("value");
        $(".alet"+id).hide(0).delay(1500).show(0);
        $(".alert"+id).show(0).delay(1500).hide(0);
    });
}

<?php

class Cart {


    //product toevoegen aan cart
    public static function addToCart($id, $quantity = 1)
    {
        if(isset($_SESSION['cart']['items'][$id])) {
            $_SESSION['cart']['items'][$id]['quantity'] += $quantity;
        }
        else {
            $item = Items::findById($id);

            $_SESSION['cart']['items'][$id] = [
                'quantity' => 1,
                'title' => $item->title,
                'price' => $item->price,
                'id' => $item->id,
            ];

            $_SESSION['cart']['restaurant'] = $item->restaurant_id;

        }

        self::calculate();
    }

    public static function diffrentRestaurant($id)
    {
        if(@$_SESSION['cart']['restaurant'] != $id){
            self::reset();
        }
    }

    //product verwijderen van cart
    public static function removeFromCart($id)
    {
        if($_SESSION['cart']['items'][$id]['quantity'] > 1) {
            $_SESSION['cart']['items'][$id]['quantity']--;
        }
        else {
            unset($_SESSION['cart']['items'][$id]);
        }

        self::calculate();
    }


    public static function get()
    {
        return $_SESSION['cart'];
    }

    //berekent het totale bedrag
    private static function calculate()
    {
        $totalPrice = 0;
        foreach($_SESSION['cart']['items'] as $key => $value) {
            $totalPrice += $value['price'] * $value['quantity'];
        }

        $_SESSION['cart']['total'] = $totalPrice;
    }

    //reset de cart
    public static function reset()
    {
        $_SESSION['cart'] = [
            'items' => [],
            'total' => 0.00,
            'restaurant' => '',
            'quantity' => 0,
        ];
    }
}
<?php
if(isset($_POST["query"]))
{
	$db = DB::prepare("SELECT * FROM restaurants WHERE name LIKE :query");
	$db->execute(['query' => '%'.$_POST['query'].'%']);

	$restaurants = $db->fetchAll(PDO::FETCH_CLASS, 'Restaurant');
}
else
{
	$restaurants = Restaurant::get();
}



if(isset($restaurants)){
	foreach($restaurants as $restaurant){

		if($restaurant->closed_at > date('H:i:s') && $restaurant->open_at < date('H:i:s')){
		?>
		<div class="col-md-4 mb-4">
			<div class="card h-100">
				<div class="card-body">
							<img class="homeproduct" width="300px" height="300px" src="images/<?= $restaurant->image?>">
					<h4 class="card-title"><?= $restaurant->name ?></h4>
				</div>
				<div class="card-footer">
					<a href="?page=items&id=<?= $restaurant->id ?>">menu</a>
						<div>open</div>
				</div>
			</div>
		</div>

<?php
// dit is voor gesloten restaurants
		}
	}
	foreach($restaurants as $restaurant){

		if($restaurant->closed_at < date('H:i:s') || $restaurant->open_at > date('H:i:s')){
		?>
		<div class="col-md-4 mb-4">
			<div class="card h-100">
				<div class="card-body">
							<img class="homeproduct" width="300px" height="300px" src="images/<?= $restaurant->image?>">
					<h4 class="card-title"><?= $restaurant->name ?></h4>
				</div>
				<div class="card-footer">
					<a href="?page=items&id=<?= $restaurant->id ?>">menu</a>
						<div>gesloten</div>
				</div>
			</div>
		</div>



<?php

		}
	}
} else {
	echo 'restaurant niet gevonden';
}
?>
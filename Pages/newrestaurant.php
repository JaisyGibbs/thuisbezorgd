<?php

App::pageAuth([App::ROLE_USER]);

if ($_SERVER['REQUEST_METHOD'] === 'POST') {

    $restaurant = Restaurant::register($_POST);
};
if (@$restaurant){
	App::redirect('restaurant');
}
?>


<div class="container">
    <div class="card card-model card-model-sm">
        <div class="card-header">
            Restaurant
        </div>
        <div class="card-body">
            <?= Restaurant::form($_POST); ?>
        </div>
    </div>
</div>

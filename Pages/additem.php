<?php

App::pageAuth(['user'], "login");

$restaurant = Restaurant::findById($_GET['id']);

$items = Items::findBy("restaurant_id", $_GET['id']);

if(!$restaurant->user_id === App::$user->id){
	App::redirect("items&id=".$_GET['id']);
}

if (isset($_POST['title'])) {
	$check= Items::newItem($_POST, $_GET['id']);
	if($check === true){
		App::redirect("items&id=".$_GET['id']);
	}
}
?>

<div class="container">
    <div class="card card-model card-model-sm">
        <div class="card-header">
            Login
        </div>
        <div class="card-body">
            <?= App::displayErrors(); ?>
            <hr>
            <?= Items::newItemForm($_POST); ?>
        </div>
    </div>
</div>


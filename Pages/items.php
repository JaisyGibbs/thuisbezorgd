<?php

App::pageAuth(['user'], "login");

$restaurant = Restaurant::findById($_GET['id']);

$items = Items::findBy("restaurant_id", $_GET['id']);

if ($_SERVER['REQUEST_METHOD'] === 'POST'){
	Items::editItem($_POST);
	App::refresh();
}
cart::diffrentRestaurant($_GET['id']);
?>


<div class="container text-center">
	<div class="row">
		<?php if($restaurant->user_id === App::$user->id){ ?>
		<a type="button" class="btn btn-info btn-lg" href="?page=additem&id=<?= $_GET['id'] ?>">Nieuw item</a>
		<?php } else { ?>
			<button class="btn btn-primary cart" data-toggle="modal" data-url="?page=cart&id=none&type=none&ajax=true" data-target="#cart">Bestelling</button>
		<?php } ?>
	</div>
	<?php
	foreach (Items::$types as $type => $key) {
		?>
		<h1><?= $key ?></h1><br>
		<div class="row">
			<?php foreach($items as $item){

			if($item->type == $type){ ?>

			    <div class="col-sm-4">
			        <?php if($restaurant->user_id === App::$user->id){ ?>
			            <img src="images/<?= $item->image ?>" class="person" data-toggle="modal" data-target="#myModal<?= $item->id?>" width="255" height="255">

			            <div class="modal fade" id="myModal<?= $item->id ?>" role="dialog">
					    	<div class="modal-dialog">
					      		<div class="modal-content">
					      			<div class="card-body">
							            <?= App::displayErrors(); ?>
							            <hr>
							            <?= Items::editItemForm($item->id) ?>
							        </div>
					      		</div>
					   		</div>
					    </div>
					    <p class="text-center"><strong><?= $item->title ?> </strong><?= $item->price ?></p>
			        <?php } else { ?>

			        	<img src="images/<?= $item->image ?>" class="person cart" data-url="?page=cart&id=<?= $item->id ?>&type=add" width="255" height="255">

			        	<button class="cart addItem" data-url="?page=cart&id=<?= $item->id ?>&type=add&ajax=true" value="<?= $item->id ?>">
			        		<?= $item->title ?> <?= $item->price ?>
			        	</button>

			        	<div id="alert" class="alert alert-success alert<?= $item->id ?>" role="alert">
	  						<strong><?= $item->title ?></strong> Toegevoegd.
						</div>

			        <?php } ?>
			    </div>

		<?php } } ?>
		</div>
<?php  } ?>
</div>

<div class="modal fade" id="cart" role="dialog">
	<div class="modal-dialog">
  		<div class="modal-content">
  			<div id="bucket"></div>
  		</div>
	</div>
</div>


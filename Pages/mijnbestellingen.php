<?php

App::pageAuth(['user'], "login");

$orders = Order::findBy('user_id', App::$user->id);

?>

<?php

?>

<div class="container">
	<table class="table">
  		<tr>
  			<th>restaurant</th>
  			<th>datum</th>
  			<th>tijd</th>
  			<th>totaal</th>
  		</tr>
  	</table>
  	<table class="table table-bordered">
  		<?php foreach($orders as $order){?>
    	<tr class="header">
      		<td><?= Restaurant::getName($order->restaurant_id) ?></td>
        	<td><?= $order->getDate($order->created_at) ?></td>
        	<td><?= $order->getTime($order->created_at) ?></td>
        	<td><?= $order->subtotal ?></td>
    	</tr>
    	<tr>
      		<td>Item</td>
        	<td>Aantal</td>
        	<td>prijs</td>
        	<td>totaal</td>
    	</tr>

    	<?= Order_item::getItems($order->id);
    	} ?>

  </table>
</div>

<script>
$(document).ready(function() {
    //Fixing jQuery Click Events for the iPad
    var ua = navigator.userAgent,
    event = (ua.match(/iPad/i)) ? "touchstart" : "click";
    if ($('.table').length > 0) {
    	$('.table .header').on(event, function() {
      		$(this).toggleClass("active", "").nextUntil('.header').css('display', function(i, v) {
   		     	return this.style.display === 'table-row' ? 'none' : 'table-row';
      		});
    	});
  	}
})
</script>
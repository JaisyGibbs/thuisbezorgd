<?php

App::pageAuth(['user'], "login");

$restaurants = Restaurant::findBy('user_id', App::$user->id);
?>



<div class="container">
	<div class="row">
		<?php if(count($restaurants) < 3){ ?>
		<a class="btn btn-primary" <?= App::link('newrestaurant') ?>>Nieuw restaurant</a>
		<?php } ?>
	</div>
	<br>
	<div class="row">
	<?php foreach($restaurants as $restaurant){ ?>
		<div class="col-md-4 mb-4">
			<div class="card h-100">
				<div class="card-body">
					<img class="homeproduct" width="300px" height="300px" src="images/<?= $restaurant->image?>">
					<h4 class="card-title"><?= $restaurant->name ?></h4>
				</div>
				<div class="card-footer">
					<a href="?page=wijzigen&id=<?= $restaurant->id ?>">wijzigen</a><br>
					<a href="">bestellingen</a><br>
					<a href="?page=items&id=<?= $restaurant->id ?>">menu items</a>
				</div>
			</div>
		</div>
	<?php } ?>
	</div>
</div>





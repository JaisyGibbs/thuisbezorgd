<?php

App::pageAuth([App::ROLE_USER]);

if(@$_GET['type'] == 'add'){
	Cart::addToCart($_GET['id']);
}

if(@$_GET['type'] == 'del'){
	Cart::removeFromCart($_GET['id']);
}

if(@$_GET['type'] == 'res'){
	Cart::reset();
}

?>
<div class="margin">
    <h2>Winkelmandje</h2>
    <!-- kijken of er wat in het winkelmandje zit -->
    <?php if($_SESSION['cart']['total'] != 0) {
        $restaurant = Restaurant::findById($_SESSION["cart"]["restaurant"]);
    ?>


    <table class="table">
        <thead>
            <tr>
                <th>Item</th>
                <th>Aantal</th>
                <th>Prijs</th>
                <th></th>
            </tr>
        </thead>
        <tbody>

        <?php foreach($_SESSION['cart']['items'] as $item) { ?>

            <tr>
                <td><?php echo $item['title']; ?></td>
                <td><?php echo $item['quantity']; ?></td>
                <td><?php echo $item['price']; ?></td>
            </tr>
        <?php } ?>
        </tbody>
    </table>

    <h3>Totaal bedrag: <?= number_format($_SESSION['cart']['total'], 2, '.', '')?></h3>
        <?php if(Http::$page == '?page=pay'){ ?>
            <?php if($restaurant->closed_at > date('H:i:s') && $restaurant->open_at < date('H:i:s')){ ?>
            <a class="btn btn-primary betalen" href="?page=pay&pay=true">Afrekenen</a>
            <?php } else { ?>
                <p>Dit restaurant is momenteel gesloten het is weer geopend om <?= $restaurant->open_at ?></p>
            <?php } ?>
        <?php } else { ?>
            <a class="btn btn-primary betalen" href="?page=pay">Betalen</a>
        <?php }
    } else {?>
        <p>Er staan geen artikelen in het Winkelmandje</p>
    <?php }?>
</div>
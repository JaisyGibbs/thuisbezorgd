<?php

App::pageAuth(['user'], "login");

$restaurant = Restaurant::findById($_GET['id']);

if($restaurant->user_id != App::$user->id){
    App::redirect('home');
}
if($_POST){
	Restaurant::edit($_POST, $restaurant->id);
	App::redirect('restaurant');
}
?>

<div class="container">
    <div class="card card-model card-model-sm">
        <div class="card-header">
            Update
        </div>
        <div class="card-body">
            <?= Restaurant::editform($restaurant->id); ?>
        </div>
    </div>
</div>
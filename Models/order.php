<?php

class Order extends Model
{
    // Set the table name
    protected $table = 'orders';

    /**
     * @Type int(11)
     */
    protected $user_id;

    /**
     * @Type int(11)
     */
    protected $restaurant_id;

    /**
     * @Type double(6,2)
     */
    protected $subtotal;


    // This method is called after filling the model with the values from the form and
    // before saving it to the database. You can add your own adjustments and checks here.
    // If a model shouldn't be saved, simply return false. Else return nothing, or true. Whatever.
    protected static function newModel($obj)
    {
        return true;
    }


    public function __construct()
    {

    }

    public function getDate($date)
    {
        return date('Y-m-d',strtotime($date));
    }

    public function getTime($date)
    {
        $time = date('H:i:s',strtotime($date));
        $parts = explode(":", $time);
        return $parts[0]. ":" . $parts[1];
    }

    public static function order(){
        try{
            $order = self::newOrder();

            if($order){
                foreach($_SESSION['cart']['items'] as $item){
                    order_item::newItem($order, $item);
                }

                cart::reset();
            }
        } catch (Exception $e) {
        echo 'Caught exception: ',  $e->getMessage(), "\n";
        }
    }

    private static function newOrder(){
        $order = new order();
        $order->user_id = app::$user->id;
        $order->restaurant_id = $_SESSION['cart']['restaurant'];
        $order->subtotal = $_SESSION['cart']['total'];
        $order->save();

        if($order->getId()){
            return $order;
        } else {
            return false;
        }

    }
}
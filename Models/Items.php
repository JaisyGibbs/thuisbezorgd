<?php

use Intervention\Image\ImageManagerStatic as Image;

class Items extends Model
{

    public static $types = [
            0 => "Hoofdgerecht",
            1 => "Bijgerecht",
            2 => "Dranken"];

    // Set the table name
    protected $table = 'items';

    /**
     * @Type varchar(255)
     */
    protected $title;

    /**
     * @Type varchar(255)
     */
    protected $description;

    /**
     * @Type varchar(255)
     */
    protected $image;

    /**
     * @Type double(5,2)
     */
    protected $price;

    /**
     * @Type varchar(255)
     */
    protected $type;

    /**
     * @Type varchar(255)
     */
    protected $restaurant_id;


    // This method is called after filling the model with the values from the form and
    // before saving it to the database. You can add your own adjustments and checks here.
    // If a model shouldn't be saved, simply return false. Else return nothing, or true. Whatever.
    protected static function newModel($obj)
    {
        return true;
    }


    public function __construct()
    {
        if(!$this->image){
            $this->image = "defaultitem.jpg";
        }
    }

    public static function editItem($post)
    {

        $item = Items::findById($post['id']);

        $item->title = $post['title'];
        $item->description = $post['description'];
        $item->price = $post['price'];
        $item->type = $post['type'];

        if($item->image === "default.jpg"){
            $item->image = "";
        } elseif ( @!!$_FILES['image']['tmp_name']) {
            $fileParts = pathinfo($_FILES['image']['name']);
            if($item->image) {
                @unlink(Http::$dirroot.'public'.DIRECTORY_SEPARATOR.'images'.DIRECTORY_SEPARATOR.$item->image);
            }
            $item->image = sha1($fileParts['filename'].microtime()).'.'.$fileParts['extension'];
            if(in_array($fileParts['extension'], ['jpg', 'jpeg', 'png'])) {
                if(move_uploaded_file($_FILES['image']['tmp_name'], Http::$dirroot.'public'.DIRECTORY_SEPARATOR.'images'.DIRECTORY_SEPARATOR.$item->image)) {
                    // the file has been moved correctly, now resize it
                    // open and resize an image file
                    $img = Image::make(Http::$dirroot.'public'.DIRECTORY_SEPARATOR.'images'.DIRECTORY_SEPARATOR.$item->image)->fit(255, 255);
                    // save file as jpg with maximum quality
                    $img->save(Http::$dirroot.'public'.DIRECTORY_SEPARATOR.'images'.DIRECTORY_SEPARATOR.$item->image, 100);
                }
            }
            else {
                // error this file ext is not allowed
            }
        }
        $item->save();

    }

    public static function newItem($post, $id)
    {

        $item = new Items();

        // if(!preg_match('/^\d{1,5}(,\d{1,2})?$/', $post['price'])){
        //     App::addError("prijs klopt niet format = 00,00");
        //     return false;
        // } else {

        $item->title = $post['title'];
        $item->description = $post['description'];
        $item->price = $post['price'];
        $item->restaurant_id = $id;
        $item->type = $post['type'];

        if($item->image === "default.jpg"){
            $item->image = "";
        } elseif ( @!!$_FILES['image']['tmp_name']) {
            $fileParts = pathinfo($_FILES['image']['name']);
            if($item->image) {
                @unlink(Http::$dirroot.'public'.DIRECTORY_SEPARATOR.'images'.DIRECTORY_SEPARATOR.$item->image);
            }
            $item->image = sha1($fileParts['filename'].microtime()).'.'.$fileParts['extension'];
            if(in_array($fileParts['extension'], ['jpg', 'jpeg', 'png'])) {
                if(move_uploaded_file($_FILES['image']['tmp_name'], Http::$dirroot.'public'.DIRECTORY_SEPARATOR.'images'.DIRECTORY_SEPARATOR.$item->image)) {
                    // the file has been moved correctly, now resize it
                    // open and resize an image file
                    $img = Image::make(Http::$dirroot.'public'.DIRECTORY_SEPARATOR.'images'.DIRECTORY_SEPARATOR.$item->image)->fit(255, 255);
                    // save file as jpg with maximum quality
                    $img->save(Http::$dirroot.'public'.DIRECTORY_SEPARATOR.'images'.DIRECTORY_SEPARATOR.$item->image, 100);
                }
            }
            else {
                // error this file ext is not allowed
            }
        }
        $item->save();

        return true;


    }

    public static function newItemForm($post = [])
    {

        $form = new Form();

        $form->addField((new FormField("title"))
            ->value(@$post['title'])
            ->placeholder("naam")
            ->required());

        $form->addField((new FormField("description"))
            ->value(@$post['description'])
            ->placeholder("omschrijving")
            ->required());

        $form->addField((new FormField("price"))
            ->value(@$post['price'])
            ->placeholder("prijs")
            ->required());

        $form->addField((new FormField("type"))
            ->type("select")
            ->value([
                        0 => "Hoofdgerecht",
                        1 => "Bijgerecht",
                        2 => "Dranken"
                    ]));

        $form->addField((new FormField("image"))
            ->type("file")
            ->placeholder("Logo"));

        return $form->getHTML();

    }

    public static function editItemForm($id)
    {

        $item = self::findById($id);

        $form = new form();

        $form->addField((new FormField("title"))
            ->value($item->title)
            ->placeholder("naam")
            ->required());

        $form->addField((new FormField("description"))
            ->value($item->description)
            ->placeholder("omschrijving")
            ->required());

        $form->addField((new FormField("price"))
            ->type("decimal")
            ->value($item->price)
            ->placeholder("prijs")
            ->required());

        $form->addField((new FormField("type"))
            ->type("select")
            ->placeholder("type")
            ->value([
                        0 => "Hoofdgerecht",
                        1 => "Bijgerecht",
                        2 => "Dranken"
                    ])
            ->required());

        $form->addField((new FormField("image"))
            ->type("file")
            ->placeholder("Logo"));

        $form->addfield((new FormField("id"))
            ->type('hidden')
            ->value($item->id));

        return $form->getHTML();

    }
}

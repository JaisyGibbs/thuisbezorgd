<?php

class order_item extends Model
{
    // Set the table name
    protected $table = 'order_items';

    /**
     * @Type int(11)
     */
    protected $quantity;

    /**
     * @Type int(11)
     */
    protected $item_id;

    /**
     * @Type int(11)
     */
    protected $order_id;

    /**
     * @Type double(5,2)
     */
    protected $price;




    // This method is called after filling the model with the values from the form and
    // before saving it to the database. You can add your own adjustments and checks here.
    // If a model shouldn't be saved, simply return false. Else return nothing, or true. Whatever.
    protected static function newModel($obj)
    {
        return true;
    }


    public function __construct()
    {

    }

    public function getTotal($order){
        $total = $order->quantity * $order->price;
        return $total;
    }

    public static function getItems($id){
        $items = self::findBy("order_id", $id);
        $html = "";
        foreach($items as $item){
            $name = Items::findById($item->item_id)->title;
            $html .= "<tr>";
            $html .= "<td>". $name ."</td>";
            $html .= "<td>". $item->quantity ."</td>";
            $html .= "<td>". $item->price ."</td>";
            $html .= "<td>". $item->getTotal($item) ."</td>";
            $html .= "</tr>";

        }
        return $html;
    }

    public static function NewItem($order, $item){

        $newItem = new order_item();
        $newItem->quantity = $item['quantity'];
        $newItem->order_id =  $order->id;
        $newItem->item_id = $item['id'];
        $newItem->price = $item['price'];
        $newItem->save();
    }
}

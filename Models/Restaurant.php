<?php

use Intervention\Image\ImageManagerStatic as Image;

class Restaurant extends Model
{
    // Set the table name
    protected $table = 'restaurants';

    // Define the relations here
    // static protected $belongsTo = [];
    // static protected $hasMany = [];

    /**
     * @Type varchar(255)
     */
    protected $name;

    /**
     * @Type int(11)
     */
    protected $user_id;

    /**
     * @Type varchar(255)
     */
    protected $street;

    /**
     * @Type varchar(255)
     */
    protected $street_number;

    /**
     * @Type varchar(255)
     */
    protected $street_number_suffix;

    /**
     * @Type varchar(255)
     */
    protected $city;

    /**
	 * @type decimal(10,8)
	 */
    protected $latitude;

    /**
	 * @type decimal(11,8)
	 */
    protected $longitude;

    /**
     * @Type varchar(255)
     */
    protected $postcode;

    /**
     * @Type varchar(255)
     */
    protected $image;

    /**
     * @Type varchar(255)
     */
    protected $open_at;

    /**
     * @Type varchar(255)
     */
    protected $closed_at;

    // This method is called after filling the model with the values from the form and
    // before saving it to the database. You can add your own adjustments and checks here.
    // If a model shouldn't be saved, simply return false. Else return nothing, or true. Whatever.

    public function __construct()
    {
        if(!$this->image){
            $this->image = "default.jpg";
        }

    }

    public static function getName($id)
    {
        return self::findById($id)->name;
    }

    public static function register($form)
    {
    	$restaurant = new Restaurant();
    	$restaurant->user_id = App::$user->id;
    	$restaurant->name = $form['name'];
	    $restaurant->street = $form['street'];
	    $restaurant->street_number = $form['street_number'];
	    $restaurant->street_number_suffix = $form['street_number_suffix'];
	    $restaurant->city = $form['city'];
	    $restaurant->postcode = $form['postcode'];
	    $restaurant->open_at = $form['open_at'] . ":00";
	    $restaurant->closed_at = $form['closed_at'] . ":00";

        if($restaurant->image === "default.jpg"){
            $restaurant->image = "";
        } elseif ( @!!$_FILES['image']['tmp_name']) {
            $fileParts = pathinfo($_FILES['image']['name']);
            if($restaurant->image) {
                @unlink(Http::$dirroot.'public'.DIRECTORY_SEPARATOR.'images'.DIRECTORY_SEPARATOR.$restaurant->image);
            }
            $restaurant->image = sha1($fileParts['filename'].microtime()).'.'.$fileParts['extension'];
            if(in_array($fileParts['extension'], ['jpg', 'jpeg', 'png'])) {
                if(move_uploaded_file($_FILES['image']['tmp_name'], Http::$dirroot.'public'.DIRECTORY_SEPARATOR.'images'.DIRECTORY_SEPARATOR.$restaurant->image)) {
                    // the file has been moved correctly, now resize it
                    // open and resize an image file
                    $img = Image::make(Http::$dirroot.'public'.DIRECTORY_SEPARATOR.'images'.DIRECTORY_SEPARATOR.$restaurant->image)->fit(300, 300);
                    // save file as jpg with maximum quality
                    $img->save(Http::$dirroot.'public'.DIRECTORY_SEPARATOR.'images'.DIRECTORY_SEPARATOR.$restaurant->image, 100);
                }
            }
            else {
                // error this file ext is not allowed
            }
        }

	    $restaurant->save();

        if($restaurant->getId()) {
            return $restaurant;
        } else {
            return false;
        }
    }

    public static function edit($form, $id)
    {
        $restaurant = self::findById($id);
        $restaurant->user_id = App::$user->id;
        $restaurant->name = $form['name'];
        $restaurant->street = $form['street'];
        $restaurant->street_number = $form['street_number'];
        $restaurant->street_number_suffix = $form['street_number_suffix'];
        $restaurant->city = $form['city'];
        $restaurant->postcode = $form['postcode'];
        $restaurant->open_at = $form['open_at'] . ":00";
        $restaurant->closed_at = $form['closed_at'] . ":00";

        if($restaurant->image === "default.jpg"){
            $restaurant->image = "";
        } elseif ( @!!$_FILES['image']['tmp_name']) {
            $fileParts = pathinfo($_FILES['image']['name']);
            if($restaurant->image) {
                @unlink(Http::$dirroot.'public'.DIRECTORY_SEPARATOR.'images'.DIRECTORY_SEPARATOR.$restaurant->image);
            }
            $restaurant->image = sha1($fileParts['filename'].microtime()).'.'.$fileParts['extension'];
            if(in_array($fileParts['extension'], ['jpg', 'jpeg', 'png'])) {
                if(move_uploaded_file($_FILES['image']['tmp_name'], Http::$dirroot.'public'.DIRECTORY_SEPARATOR.'images'.DIRECTORY_SEPARATOR.$restaurant->image)) {
                    // the file has been moved correctly, now resize it
                    // open and resize an image file
                    $img = Image::make(Http::$dirroot.'public'.DIRECTORY_SEPARATOR.'images'.DIRECTORY_SEPARATOR.$restaurant->image)->fit(300, 300);
                    // save file as jpg with maximum quality
                    $img->save(Http::$dirroot.'public'.DIRECTORY_SEPARATOR.'images'.DIRECTORY_SEPARATOR.$restaurant->image, 100);
                }
            }
            else {
                // error this file ext is not allowed
            }
        }

        $restaurant->save();

        if($restaurant->getId()) {
            return $restaurant;
        } else {
            return false;
        }
    }


    public static function form($post = [])
    {
    	$form = new Form();

    	$form->addField((new FormField("name"))
            ->value(@$post['name'])
            ->placeholder("naam")
            ->required());

        $form->addField((new FormField("street"))
            ->value(@$post['street'])
            ->placeholder("Straat naam")
            ->required());

        $form->addField((new FormField("street_number"))
            ->type("number")
        	->value(@$post['street_number'])
            ->placeholder("Straat nummer")
            ->required());

        $form->addField((new FormField("street_number_suffix"))
            ->value(@$post['street_number_suffix'])
            ->placeholder("Straat voegsel"));

        $form->addField((new FormField("postcode"))
            ->value(@$post['postcode'])
            ->placeholder("Postcode")
            ->required());

        $form->addField((new FormField("city"))
            ->value(@$post['city'])
            ->placeholder("Stad")
            ->required());

        $form->addField((new FormField("open_at"))
        	->type("time")
            ->value(@$post['time'])
            ->placeholder("openingstijd")
            ->required());

        $form->addField((new FormField("closed_at"))
        	->type("time")
            ->value(@$post['time'])
            ->placeholder("sltuitingstijd")
            ->required());

        $form->addField((new FormField("image"))
            ->type("file")
            ->placeholder("Logo"));

        return $form->getHTML();
    }

    public static function editform($id)
    {

        $restaurant = Restaurant::findById($id);

        $form = new Form();

        $form->addField((new FormField("name"))
            ->value($restaurant->name)
            ->placeholder("naam")
            ->required());

        $form->addField((new FormField("street"))
            ->value($restaurant->street)
            ->placeholder("Straat naam")
            ->required());

        $form->addField((new FormField("street_number"))
            ->type("number")
            ->value($restaurant->street_number)
            ->placeholder("Straat nummer")
            ->required());

        $form->addField((new FormField("street_number_suffix"))
            ->value($restaurant->street_number_suffix)
            ->placeholder("Straat voegsel"));

        $form->addField((new FormField("postcode"))
            ->value($restaurant->postcode)
            ->placeholder("Postcode")
            ->required());

        $form->addField((new FormField("city"))
            ->value($restaurant->city)
            ->placeholder("Stad")
            ->required());

        $form->addField((new FormField("open_at"))
            ->type("time")
            ->value($restaurant->open_at)
            ->placeholder("openingstijd")
            ->required());

        $form->addField((new FormField("closed_at"))
            ->type("time")
            ->value($restaurant->closed_at)
            ->placeholder("sltuitingstijd")
            ->required());

        $form->addField((new FormField("image"))
            ->type("file")
            ->placeholder("Logo"));

        return $form->getHTML();
    }


    protected static function newModel($obj)
    {
		return true;
    }

}

